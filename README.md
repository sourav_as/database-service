# Completed
1. Connect to multiple services.
2. Get commit details from git.
3. Email the user who made the last commit to the file in case of errors.
4. Send database name in case of clinics in the email.
5. Idempotent queries for mysql.
---
# Pending
1. Stored procedure & idempotent queries for postgres.
2. Get permissions for all qa db from devops.
3. Add project to QA Environment.
4. Verify logs, git working in QA environment.
5. Environment

build & deploy -> restart.
-d environment 
based on this environment -> base url.

cache or api to be hit:
release related challenges.
api to be hit -> 

csv file: prefix, key, map or set
onboarding -> cache refreshing -> use that here.

scheduler job to be created.
