package com.thp.database.constants;

public enum ErrorCodes {

    ERROR_ESTABLISHING_CONNECTION(-7999, "Error while establishing database connection"),
    ERROR_EXECUTING_SQL_FILE(-7998, "Error while executing SQL file"),

    ERROR_DDL_DML_NOT_FOUND(-7995, "Error while searching for DDL/DML/Script folders"),
    ERROR_READING_FILE_CONTENTS(-7994, "Error reading file contents"),
    ERROR_CLOSING_CONNECTION(-7993, "Error closing connection"),
    INVALID_FILE_NAME_FORMAT(-7992, "Invalid file name format"),
    ERROR_EXECUTING_REFRESH_SCRIPT(-7988, "Failed to execute Refresh Script"),
    INVALID_NUMBER_OF_FIELDS_IN_REFRESH_SCRIPT(-7987, "Invalid Number of Fields in Refresh Script"),
    ERROR_STATUS_FROM_DELETE_CACHE_API(-7986, "Error Status From Delete Cache API"),
    ERROR_CLONING_GIT(-7985, "Error Cloning Git Repository"),

    ;

    private final int errorCode;
    private final String errorMsg;

    ErrorCodes(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return Integer.toString(errorCode);
    }

}