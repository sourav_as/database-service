package com.thp.database.constants;

public class Constants {
    public static final String DML = "DML";
    public static final String DDL = "DDL";
    public static final String REVERT = "REVERT";
    public static final String REFRESH = "refresh";
    public static final String CLINICS = "clinics";
    public static final String REQ_MAPPING_EXECUTE_SCRIPT = "/api";
    public static final String NOT_APPLICABLE = "NA";
    public static final String SEND_INTERNAL_NOTIFICATION_URL = "/ConsumerNotification/internalNotifications";
    public static final String GLOBAL_SERVICE_USER_ID = "globalServiceUserId";
    public static final String GLOBAL_SERVICE_USER_TOKEN = "globalServiceUserToken";
    public static final String SPRING_DATASOURCE_URL = "spring.datasource.url.";
    public static final String SPRING_DATASOURCE_USERNAME = "spring.datasource.username.";
    public static final String SPRING_DATASOURCE_PASSWORD = "spring.datasource.password.";
    public static final String SPRING_DATASOURCE_DRIVER = "spring.datasource.driverClass.";
    public static final String DB_DATASOURCE_ENABLESSL = "db.datasource.enablessl.";
    public static final String DB_DATASOURCE_SSLROOTCERT = "db.datasource.sslrootcert.";
    public static final String DB_DATASOURCE_USESSL = "db.datasource.usessl.";
    public static final String DB_DATASOURCE_SSLMODE = "db.datasource.sslmode.";
    public static final String CONSUMER_SERVER_IP = "consumer_server_ip";
    public static final String CLINICS_GLOBAL = "CLINICS_GLOBAL";
    public static final String GLOBAL_SERVICE = "global";
    public static final String TENANT_DS_NAMES = "tenant.ds.names";
    public static final String SEPARATOR = ",";
    public static final String CONSUMER_ONBOARDING_API_V_1_DELETE_CACHE = "/consumerOnboarding/api/v1/deleteCache";
    public static final String SERVER_BASE_URL = "server.base.url";
    public static final String SQL_COMMAND_SEPARATOR = "---";

    private Constants() {
    }
}
