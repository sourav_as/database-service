package com.thp.database.constants;

public enum SuccessCodes {

    SCRIPT_EXECUTION_SUCCESS(8000, "Scripts executed successfully");

    private final int successCode;
    private final String successMsg;

    SuccessCodes(int successCode, String successMsg) {
        this.successCode = successCode;
        this.successMsg = successMsg;
    }

    public int getSuccessCode() {
        return successCode;
    }

    public String getSuccessMsg() {
        return successMsg;
    }

}
