package com.thp.database.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@SpringBootConfiguration
@PropertySource("classpath:database-testing.properties")
public class DatabasePropertyConfiguration {

    @Autowired
    private Environment env;

    public String getProperty(String property) {
        return env.getProperty(property);
    }
}
