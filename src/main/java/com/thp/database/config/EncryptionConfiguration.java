package com.thp.database.config;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.hibernate5.encryptor.HibernatePBEStringEncryptor;
import org.jasypt.salt.ZeroSaltGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootConfiguration
public class EncryptionConfiguration {

    @Autowired
    private Environment environment;

    @Bean(name = "hibernateStringEncryptor")
    public HibernatePBEStringEncryptor stringEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        encryptor.setAlgorithm("PBEWithMD5AndTripleDES");
        encryptor.setPassword(environment.getProperty("db.encryption.password"));
        encryptor.setPoolSize(Integer.parseInt(environment.getProperty("db.encryption.pool.size")));
        encryptor.setSaltGenerator(new ZeroSaltGenerator());
        HibernatePBEStringEncryptor stringEncryptor = new HibernatePBEStringEncryptor();
        stringEncryptor.setRegisteredName("hibernateStringEncryptor");
        stringEncryptor.setEncryptor(encryptor);
        return stringEncryptor;
    }
}