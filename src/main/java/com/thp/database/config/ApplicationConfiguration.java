package com.thp.database.config;

import com.thp.common.utils.filters.RequestResponseLoggingFilter;
import com.thp.common.utils.filters.TokenValidationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;

@Configuration
@DependsOn("PropertyReader")
public class ApplicationConfiguration {

    Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);
    @Autowired
    private Environment env;

    @Bean
    public FilterRegistrationBean<RequestResponseLoggingFilter> requestResponseLoggingFilter() {

        FilterRegistrationBean<RequestResponseLoggingFilter> requestResponseLoggingFilterBean = new FilterRegistrationBean<>();
        requestResponseLoggingFilterBean.setFilter(new RequestResponseLoggingFilter());
        requestResponseLoggingFilterBean.addUrlPatterns("/*");
        requestResponseLoggingFilterBean.setName("request-response-logging-filter");
        requestResponseLoggingFilterBean.setOrder(1);
        return requestResponseLoggingFilterBean;
    }

    @Bean
    public FilterRegistrationBean<TokenValidationFilter> getTokenValidationFilter() {

        FilterRegistrationBean<TokenValidationFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new TokenValidationFilter());
        registration.addUrlPatterns("/*");
        registration.setName("TokenValidationFilter");
        registration.addInitParameter("guestUserExcludePatterns", "");
        registration.setOrder(2);
        return registration;
    }

}
