package com.thp.database.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@SpringBootConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
        "com.thp.database.repository"}, entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "txManager")
public class DBConfiguration {

    public static final String JPA_BASE_PACKAGE = "com.thp.database.persistence";

    @Value("${spring.datasource.driverClass}")
    private String driver;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.max-active}")
    private Integer maxActive;

    @Value("${spring.datasource.max-idle}")
    private Integer maxIdle;

    @Autowired
    private Environment env;

    @Bean(name = "dataSource")
    public HikariDataSource dataSource() {
        HikariDataSource hikariDataSource = DataSourceBuilder.create().type(HikariDataSource.class).build();
        hikariDataSource.setDriverClassName(driver);
        hikariDataSource.setJdbcUrl(url);
        hikariDataSource.setUsername(username);
        hikariDataSource.setPassword(password);
        hikariDataSource.setMaximumPoolSize(maxActive);
        hikariDataSource.setMinimumIdle(maxIdle);
        hikariDataSource.setConnectionTestQuery("SELECT 1");
        hikariDataSource.setMaxLifetime(env.getProperty("spring.datasource.maxLifetime", Long.class));
        hikariDataSource.setConnectionTimeout(env.getProperty("spring.datasource.connectionTimeout", Long.class));
        hikariDataSource.setIdleTimeout(env.getProperty("spring.datasource.idleTimeout", Long.class));
        hikariDataSource.setAutoCommit(env.getProperty("spring.datasource.autoCommit", Boolean.class));
        hikariDataSource
                .setLeakDetectionThreshold(env.getProperty("spring.datasource.leakDetectionThreshold", Long.class));
        return hikariDataSource;
    }

    @Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean mysqlEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                            @Qualifier("dataSource") HikariDataSource dataSource) {
        return builder.dataSource(dataSource).packages(JPA_BASE_PACKAGE).persistenceUnit("mysql").build();
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(JPA_BASE_PACKAGE);
        sessionFactory.setHibernateProperties(jpaHibernateProperties());
        return sessionFactory;
    }

    @Bean
    @Primary
    public HibernateTransactionManager getTransactionManager() {
        return new HibernateTransactionManager(sessionFactory().getObject());
    }

    @Bean(name = "txManager")
    public PlatformTransactionManager jpaTransactionManager(
            @Qualifier(value = "entityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    private Properties jpaHibernateProperties() {

        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
        properties.setProperty("current_session_context_class",
                env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
        return properties;
    }

}
