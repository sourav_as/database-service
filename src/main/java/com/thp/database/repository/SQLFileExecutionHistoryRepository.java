package com.thp.database.repository;


import com.thp.database.persistence.SQLFileExecutionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SQLFileExecutionHistoryRepository extends JpaRepository<SQLFileExecutionHistory, Long> {

}
