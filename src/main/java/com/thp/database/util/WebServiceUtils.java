package com.thp.database.util;

import com.thp.common.utils.response.WebServiceResponse;
import com.thp.common.utils.utility.PropertyReader;
import com.thp.database.constants.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class WebServiceUtils {

    public static final int PREFIX_POSITION = 0;
    public static final int KEY_POSITION = 1;
    public static final int TYPE_POSITION = 2;

    public boolean deleteCache(String[] parameters) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("prefix", parameters[PREFIX_POSITION]);
            if (parameters.length > KEY_POSITION)
                params.put("key", parameters[KEY_POSITION]);
            if (parameters.length > TYPE_POSITION)
                params.put("type", parameters[TYPE_POSITION]);
            WebServiceResponse webServiceResponse = com.thp.common.utils.service.WebServiceUtils.sendGetRequest(
                    PropertyReader.getProperty(Constants.SERVER_BASE_URL) + Constants.CONSUMER_ONBOARDING_API_V_1_DELETE_CACHE,
                    null, params);
            if (Objects.nonNull(webServiceResponse) && HttpStatus.OK.value() == webServiceResponse.getHttpStatus()) {
                return true;
            }
        } catch (Exception e) {
            log.error("Exception in delete cache {}", parameters[PREFIX_POSITION]);
        }
        return false;
    }
}
