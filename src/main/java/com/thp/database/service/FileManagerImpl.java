package com.thp.database.service;

import com.thp.database.constants.Constants;
import com.thp.database.constants.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
public class FileManagerImpl implements FileManager {
    Comparator<File> comparatorForIntegerFileNames = Comparator.comparingInt(o -> Integer.parseInt(o.getName().split("\\.")[0]));
    Comparator<File> comparatorForStringFileNames = Comparator.comparing(File::getName);
    @Value("${sql.file.path}")
    private String baseFilePath;

    @Value("${git.local.path}")
    private String clonePath;

    @Value("${revert.file.path}")
    private String revertFilePath;
    @Autowired
    private NotificationService notificationService;

    @Override
    public File[] sort(File[] filesOrFolders) {
        if (filesOrFolders == null || filesOrFolders.length == 0) {
            return new File[0];
        }
        if (filesOrFolders[0].isFile()) {
            log.info("Sorting files.");
            Arrays.sort(filesOrFolders, comparatorForIntegerFileNames);
        } else {
            log.info("Sorting folders.");
            Arrays.sort(filesOrFolders, comparatorForStringFileNames);
        }
        return filesOrFolders;
    }

    @Override
    public File[] getServicesList() {
        log.info("Fetching initial services list.");
        return new File(clonePath + baseFilePath).listFiles(File::isDirectory);
    }

    @Override
    public File getRefreshScript() {
        log.info("Fetching refresh script list.");
        return new File(clonePath + baseFilePath).listFiles(File::isFile)[0];
    }

    public File getRevertSQLFile(File sqlFile) {
        log.info("Fetching revert sql file for file: {}", sqlFile.getName());
        if (sqlFile.getParentFile().getName().contains(Constants.REVERT)) {
            log.error("Revert file execution failed.");
            log.info("Fetching revert sql file cancelled.");
            return null;
        }
        return new File(sqlFile.getParent().concat(revertFilePath).concat(sqlFile.getName()));

    }

    @Override
    public String readFileContents(File file) {
        log.info("Reading file contents for file: {}", file.getName());
        List<String> fileContents = new ArrayList<>();
        try {
            fileContents = Files.readAllLines(Paths.get(file.getPath()));
        } catch (IOException e) {
            log.error("Error while reading file contents for file: {}", file.getName());
            notificationService.sendNotification(file, "", ErrorCodes.ERROR_READING_FILE_CONTENTS);
        }
        return fileContents.toString();
    }

    @Override
    public String getOperationType(File file) {
        if (file.getParentFile().getName().contains(Constants.DDL))
            return Constants.DDL;
        else if (file.getParentFile().getName().contains(Constants.DML))
            return Constants.DML;
        else if (file.getParentFile().getName().contains(Constants.REVERT))
            return Constants.REVERT;
        else if (file.getName().contains(Constants.REFRESH)) {
            return Constants.REFRESH;
        } else {
            log.error("Error while fetching operation type for file: {}", file.getName());
            notificationService.sendNotification(file, "", ErrorCodes.ERROR_DDL_DML_NOT_FOUND);
            return null;
        }
    }
}
