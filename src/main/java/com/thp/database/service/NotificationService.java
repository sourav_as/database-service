package com.thp.database.service;


import com.thp.database.constants.ErrorCodes;

import java.io.File;

public interface NotificationService {
    void sendNotification(File file, String database, ErrorCodes error);
}
