package com.thp.database.service;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service("localCloneManager")
@Slf4j
public class GitCloneManagerLocal implements GitCloneManager {

    @Override
    public void terminate(Git git) {
        if (Objects.nonNull(git))
            git.close();
    }

    @Override
    public Git cloneRepository() {
        Path p = Paths.get("./.git");
        try {
            Files.createDirectories(p);

        } catch (IOException e) {

            log.error(e.getLocalizedMessage());
        }
        RepositoryBuilder repositoryBuilder = new RepositoryBuilder();
        final File directory = p.toFile();
        repositoryBuilder.addCeilingDirectory(directory);
        repositoryBuilder.findGitDir(directory);
        try {
            return new Git(repositoryBuilder.build());
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }
}
