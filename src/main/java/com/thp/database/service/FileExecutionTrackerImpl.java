package com.thp.database.service;

import com.thp.database.persistence.SQLFileExecutionHistory;
import com.thp.database.repository.SQLFileExecutionHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Instant;
import java.util.Date;

@Service
@Slf4j
public class FileExecutionTrackerImpl implements FileExecutionTracker {

    @Autowired
    private SQLFileExecutionHistoryRepository repository;

    @Autowired
    private FileManager fileManager;

    @Override
    public void updateFileExecutionStatus(File file, String service, Boolean status, String exception, String author) {
        log.info("Saving file details in database for file: {} ", file.getName());
        SQLFileExecutionHistory sqlFileExecutionHistory = SQLFileExecutionHistory.builder()
                .fileName(file.getName())
                .service(service)
                .operationType(fileManager.getOperationType(file))
                .status(status)
                .fileContents(fileManager.readFileContents(file))
                .exception(exception)
                .author(author)
                .createdTimestamp(Date.from(Instant.now()))
                .modifiedTimestamp(Date.from(Instant.now()))
                .build();
        repository.save(sqlFileExecutionHistory);
        log.info("Saved file execution status in database for file:{}.", file.getName());
    }
}
