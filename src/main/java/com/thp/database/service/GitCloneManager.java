package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import org.eclipse.jgit.api.Git;

public interface GitCloneManager {
    void terminate(Git git);

    Git cloneRepository() throws THPException;
}
