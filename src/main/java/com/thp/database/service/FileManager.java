package com.thp.database.service;

import java.io.File;

public interface FileManager {

    File[] sort(File[] service);

    File[] getServicesList();

    File getRefreshScript();

    File getRevertSQLFile(File sqlFile);

    String readFileContents(File file);

    String getOperationType(File file);
}
