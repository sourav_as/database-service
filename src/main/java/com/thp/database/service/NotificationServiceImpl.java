package com.thp.database.service;

import com.thp.common.utils.constants.CommonUtilsConstants;
import com.thp.common.utils.response.WebServiceResponse;
import com.thp.common.utils.service.WebServiceUtils;
import com.thp.common.utils.utility.PropertyReader;
import com.thp.database.constants.Constants;
import com.thp.database.constants.ErrorCodes;
import com.thp.database.dto.CommitDetails;
import com.thp.database.dto.Email;
import com.thp.database.dto.FileDetails;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.thp.database.constants.ErrorCodes.INVALID_FILE_NAME_FORMAT;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private static final String SERVER_URL;


    static {
        SERVER_URL = StringUtils.isNotBlank(PropertyReader.getProperty(Constants.CONSUMER_SERVER_IP))
                ? PropertyReader.getProperty(Constants.CONSUMER_SERVER_IP)
                : PropertyReader.getProperty(CommonUtilsConstants.SERVER_BASE_URL_INTERNAL_PROPERTY);
    }

    @Autowired
    private GitManager gitManager;
    @Value("${email.address}")
    private String defaultEmailAddress;
    @Value("${email.subject}")
    private String subject;

    @Override
    public void sendNotification(File file, String database, ErrorCodes error) {
        log.info("Sending notification for exception: {}", error.getErrorMsg());
        FileDetails fileDetails = new FileDetails();
        CommitDetails commitDetails = new CommitDetails();
        if (Objects.nonNull(file)) {
            try {
                fileDetails = new FileDetails(file.getName(), file.getPath(), database);
                commitDetails = gitManager.fetchDetailsFromGit(file.getPath());
            } catch (Exception e) {
                log.error(INVALID_FILE_NAME_FORMAT.getErrorMsg());
            }
        }
        sendEmail(createEmail(error, fileDetails, commitDetails), commitDetails.getEmail());
    }

    private String createEmail(ErrorCodes error, FileDetails fileDetails, CommitDetails commitDetails) {
        return Email.builder()
                .errorCode(error.getErrorCode())
                .errorMessage(error.getErrorMsg())
                .fileDetails(fileDetails)
                .commitDetails(commitDetails)
                .build().toString();
    }

    private void sendEmail(String message, String email) {
        try {
            log.info("Sending Email : {}", email);
            Map<String, Object> headers = getEmailHeaders();
            Map<String, Object> request = getEmailRequest(message, email);
            WebServiceResponse webServiceResponse = WebServiceUtils
                    .sendMultipartRequest(SERVER_URL + Constants.SEND_INTERNAL_NOTIFICATION_URL, headers, request, null, null);
            if (Objects.nonNull(webServiceResponse)) {
                if (HttpStatus.OK.value() == webServiceResponse.getHttpStatus()) {
                    log.info("Status code equals 200 -> ResponseData : {}", webServiceResponse.getResponse());
                } else {
                    log.error("Status code not equals 200 -> ResponseData : {}", webServiceResponse);
                }
            }
        } catch (Exception e) {
            log.error("Error in sending notifications ", e);
        }

    }

    private Map<String, Object> getEmailHeaders() {
        Map<String, Object> headers = new HashMap<>();
        headers.put(CommonUtilsConstants.HEADER_PARAM_CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA);
        headers.put(CommonUtilsConstants.HEADER_PARAM_USER_ID, PropertyReader.getProperty(Constants.GLOBAL_SERVICE_USER_ID));
        headers.put(CommonUtilsConstants.HEADER_PARAM_AUTH_TOKEN,
                PropertyReader.getProperty(Constants.GLOBAL_SERVICE_USER_TOKEN));
        return headers;
    }

    private Map<String, Object> getEmailRequest(String message, String emailAddress) {
        Map<String, Object> request = new HashMap<>();
        request.put("isEmail", true);
        if (Objects.nonNull(emailAddress) && !emailAddress.equals(Constants.NOT_APPLICABLE))
            request.put("toAddress", emailAddress);
        else {
            log.info("No email found for file. Using default email: {}", defaultEmailAddress);
            request.put("toAddress", defaultEmailAddress);
        }
        request.put("subject", subject);
        request.put("message", message);
        request.put("contentType", "text");
        return request;
    }
}
