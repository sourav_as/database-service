package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.database.constants.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Service("remoteCloneManager")
@Slf4j
public class GitCloneManagerRemote implements GitCloneManager {
    @Value("${git.local.path}")
    private String localPath;
    @Value("${git.remote.path}")
    private String remotePath;
    @Value("${git.branch.name}")
    private String branchName;

    @Override
    public void terminate(Git git) {
        try {
            if (Objects.nonNull(git))
                git.close();
            FileUtils.deleteDirectory(new File(localPath));
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
    }


    @Override
    public Git cloneRepository() throws THPException {
        try {
            FileUtils.deleteDirectory(new File(localPath));
            File localTempPath = new File(localPath);
            log.info("Cloning from {} to {}", remotePath, localTempPath);
            return Git.cloneRepository()
                    .setURI(remotePath)
                    .setDirectory(localTempPath)
                    .setBranch(branchName)
                    .call();
        } catch (Exception e) {
            log.error("Error Cloning Repository");
            throw new THPException(ErrorCodes.ERROR_CLONING_GIT.getErrorCode(), ErrorCodes.ERROR_CLONING_GIT.getErrorMsg());
        }
    }
}
