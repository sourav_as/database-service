package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;

import java.io.File;

public interface ScriptExecutor {
    void execute(File service, String clinicName) throws THPException;
}
