package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.common.utils.utility.PropertyReader;
import com.thp.database.constants.Constants;
import com.thp.database.constants.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

@Service
@Slf4j
public class ConnectionManagerImpl implements ConnectionManager, Closeable {


    @Autowired
    private NotificationService notificationService;

    private Connection connection;

    @Override
    public Connection getConnection(File service, String name) throws THPException {
        try {
            log.info("Trying to connect to database for service:{}", name);
            connection = connectToService(service.getName(), name);
            log.info("Connection established.");
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            THPException thpException = new THPException(ErrorCodes.ERROR_ESTABLISHING_CONNECTION.getErrorCode(), ErrorCodes.ERROR_ESTABLISHING_CONNECTION.getErrorMsg());
            notificationService.sendNotification(null, "", ErrorCodes.ERROR_ESTABLISHING_CONNECTION);
            throw thpException;
        }
        return connection;
    }

    private Connection connectToService(String service, String globalOrTenant) throws ClassNotFoundException, SQLException {
        Class.forName(PropertyReader.getProperty(Constants.SPRING_DATASOURCE_DRIVER.concat(service)));
        String url = (PropertyReader.getProperty(Constants.SPRING_DATASOURCE_URL.concat(service))).concat(globalOrTenant);
        String username = PropertyReader.getProperty(Constants.SPRING_DATASOURCE_USERNAME.concat(service));
        String password = PropertyReader.getProperty(Constants.SPRING_DATASOURCE_PASSWORD.concat(service));
        Properties properties = createProperties(service, url, username, password);
        return DriverManager.getConnection(
                url,
                properties);
    }

    private Properties createProperties(String service, String url, String username, String password) {
        Properties properties = new Properties();
        properties.setProperty("url", url);
        properties.setProperty("user", username);
        properties.setProperty("password", password);
        if (BooleanUtils.isTrue(PropertyReader.getProperty(Constants.DB_DATASOURCE_ENABLESSL.concat(service), Boolean.class))) {
            properties.setProperty("sslRootCert", PropertyReader.getProperty(Constants.DB_DATASOURCE_SSLROOTCERT.concat(service)));
            properties.setProperty("ssl", PropertyReader.getProperty(Constants.DB_DATASOURCE_USESSL.concat(service)));
            properties.setProperty("sslMode", PropertyReader.getProperty(Constants.DB_DATASOURCE_SSLMODE.concat(service)));
        }
        return properties;
    }

    @Override
    public void closeConnection(Connection connection) throws THPException {
        try {
            log.info("Trying to close connection.");
            if (Objects.nonNull(connection)) {
                connection.close();
            }
            log.info("Connection closed.");
        } catch (SQLException e) {
            THPException thpException = new THPException(ErrorCodes.ERROR_CLOSING_CONNECTION.getErrorCode(), ErrorCodes.ERROR_CLOSING_CONNECTION.getErrorMsg());
            notificationService.sendNotification(null, "", ErrorCodes.ERROR_CLOSING_CONNECTION);
            throw thpException;
        }
    }

    @Override
    public void close() throws IOException {
        try {
            closeConnection(connection);
        } catch (THPException e) {
            log.error("Error while closing connection.");
        }
    }
}
