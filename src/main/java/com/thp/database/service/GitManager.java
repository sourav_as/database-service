package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.database.dto.CommitDetails;
import org.eclipse.jgit.api.errors.GitAPIException;

public interface GitManager {
    CommitDetails fetchDetailsFromGit(String filePath) throws GitAPIException;

    String getAuthor();

    void initialize() throws THPException;

    void terminate();
}
