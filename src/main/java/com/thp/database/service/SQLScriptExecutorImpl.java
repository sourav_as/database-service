package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.database.constants.Constants;
import com.thp.database.constants.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.List;
import java.util.Objects;

@Service("sqlScriptExecutor")
@Slf4j
public class SQLScriptExecutorImpl implements ScriptExecutor {

    @Autowired
    private FileManager fileManager;
    @Autowired
    private ConnectionManager connectionManager;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private FileExecutionTracker fileExecutionTracker;
    @Autowired
    private GitManager gitManager;
    private String databaseName;

    @Override
    public void execute(File service, String clinicName) throws THPException {
        databaseName = clinicName;
        File[] folders = fileManager.sort(service.listFiles(File::isDirectory));
        Connection connection = connectionManager.getConnection(service, clinicName);
        log.info("Starting execution of main folder for service: {}.", service.getName());
        executeFolders(service, folders, connection);
        log.info("Finished execution of main folder for service: {}.", service.getName());
        connectionManager.closeConnection(connection);
    }

    private void executeFolders(File service, File[] folders, Connection connection) {
        for (File folder : folders) {
            File[] sqlFiles = fileManager.sort(folder.listFiles(File::isFile));
            if (sqlFiles.length == 0) continue;
            log.info("Starting execution of files for service: {}", service.getName());
            executeFiles(service, connection, sqlFiles);
            log.info("Finished execution of files for service: {}", service.getName());
        }
    }

    private void executeFiles(File service, Connection connection, File[] sqlFiles) {
        for (File sqlFile : sqlFiles) {
            log.info("Executing file: {}", sqlFile.getName());
            executeSQLScript(service, connection, sqlFile);
            log.info("Finished executing file: {}", sqlFile.getName());
        }
    }

    private void executeSQLScript(File service, Connection connection, File sqlFile) {
        if (Objects.isNull(sqlFile) || !sqlFile.exists()) {
            return;
        }
        try {
            log.info("Starting script runner for file: {}", sqlFile);
            ScriptRunner scriptRunner = createScriptRunner(connection);
            List<String> lines = Files.readAllLines(Paths.get(sqlFile.getPath()));
            executeEachStatement(scriptRunner, lines);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            notificationService.sendNotification(sqlFile, databaseName, ErrorCodes.ERROR_EXECUTING_SQL_FILE);
            fileExecutionTracker.updateFileExecutionStatus(sqlFile, service.getName(), Boolean.FALSE, e.getLocalizedMessage(), gitManager.getAuthor());
            executeSQLScript(service, connection, fileManager.getRevertSQLFile(sqlFile));
            return;
        }
        fileExecutionTracker.updateFileExecutionStatus(sqlFile, service.getName(), Boolean.TRUE, null, null);
    }

    private ScriptRunner createScriptRunner(Connection connection) {
        ScriptRunner scriptRunner = new ScriptRunner(connection);
        scriptRunner.setStopOnError(true);
        scriptRunner.setSendFullScript(true);
        return scriptRunner;
    }

    private void executeEachStatement(ScriptRunner scriptRunner, List<String> lines) throws IOException {
        StringBuilder sqlCommand = new StringBuilder();
        for (String line : lines) {
            if (line.equals(Constants.SQL_COMMAND_SEPARATOR)) {
                executeSingleStatement(scriptRunner, sqlCommand);
                sqlCommand = new StringBuilder();
            } else {
                appendToCurrentCommand(sqlCommand, line);
            }
        }
        if (!sqlCommand.toString().isEmpty()) {
            executeSingleStatement(scriptRunner, sqlCommand);
        }
    }

    private void appendToCurrentCommand(StringBuilder sqlCommand, String line) {
        sqlCommand.append(line);
        sqlCommand.append(System.lineSeparator());
    }

    private void executeSingleStatement(ScriptRunner scriptRunner, StringBuilder sqlCommand) throws IOException {
        Reader reader = new StringReader(sqlCommand.toString());
        scriptRunner.runScript(reader);
        reader.close();
    }
}
