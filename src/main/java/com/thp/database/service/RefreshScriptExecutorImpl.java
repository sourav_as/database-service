package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.database.constants.ErrorCodes;
import com.thp.database.util.WebServiceUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service("refreshScriptExecutor")
@Slf4j
public class RefreshScriptExecutorImpl implements ScriptExecutor {
    public static final int MINIMUM_REQUIRED_PARAMETERS = 1;
    @Autowired
    WebServiceUtils webserviceUtils;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private FileExecutionTracker fileExecutionTracker;
    @Autowired
    private GitManager gitManager;

    @Override
    public void execute(File refreshScript, String clinicName) throws THPException {
        boolean isSuccessful = true;
        try {
            log.info("Executing script: {}", refreshScript.getName());
            List<String> lines = Files.readAllLines(Paths.get(refreshScript.getPath()));
            for (String line : lines) {
                log.info("Executing script line: {}", line);
                String[] parameters = line.split(",");
                if (!isValid(refreshScript, parameters)) {
                    isSuccessful = false;
                    continue;
                }
                boolean status = webserviceUtils.deleteCache(parameters);
                if (!status) {
                    notificationService.sendNotification(refreshScript, "", ErrorCodes.ERROR_STATUS_FROM_DELETE_CACHE_API);
                    isSuccessful = false;
                }
            }
        } catch (Exception e) {
            notificationService.sendNotification(refreshScript, "", ErrorCodes.ERROR_EXECUTING_REFRESH_SCRIPT);
            isSuccessful = false;
        }
        updateFileExecutionStatus(refreshScript, isSuccessful);
    }

    private boolean isValid(File refreshScript, String[] parameters) {
        if (parameters.length < MINIMUM_REQUIRED_PARAMETERS) {
            log.error("Invalid number of parameters for script.");
            notificationService.sendNotification(refreshScript, "", ErrorCodes.INVALID_NUMBER_OF_FIELDS_IN_REFRESH_SCRIPT);
            return false;
        }
        return true;
    }

    private void updateFileExecutionStatus(File refreshScript, boolean isSuccessful) {
        if (isSuccessful) {
            fileExecutionTracker.updateFileExecutionStatus(refreshScript, refreshScript.getName(), true, null, null);
        } else {
            fileExecutionTracker.updateFileExecutionStatus(refreshScript, refreshScript.getName(), false, ErrorCodes.ERROR_EXECUTING_REFRESH_SCRIPT.getErrorMsg(), gitManager.getAuthor());
        }
    }
}
