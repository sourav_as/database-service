package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;
import com.thp.database.dto.CommitDetails;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
@Slf4j
public class GitManagerImpl implements GitManager {
    @Value("${git.local.path}")
    private String localPath;
    private String author;
    private Git git;
    @Autowired
    @Qualifier("localCloneManager")
    private GitCloneManager gitCloneManager;

    @Override
    public void initialize() throws THPException {
        git = gitCloneManager.cloneRepository();
    }

    @Override
    public void terminate() {
        gitCloneManager.terminate(git);
    }


    @Override
    public CommitDetails fetchDetailsFromGit(String filePath) throws GitAPIException {
        if (Objects.isNull(git)) {
            return new CommitDetails();
        }
        filePath = getRelativePath(filePath);
        Iterable<RevCommit> iterable = git.log().addPath(filePath).call();
        RevCommit latestCommit = iterable.iterator().next();
        PersonIdent authorIdentity = latestCommit.getAuthorIdent();
        CommitDetails commitDetails = new CommitDetails(authorIdentity.getName(), authorIdentity.getEmailAddress(), authorIdentity.getWhen().toString(), latestCommit.getFullMessage());
        author = authorIdentity.getName();
        log.info("Fetched commit details for file: {}", filePath);
        log.info("Commit details: {}", commitDetails);
        return commitDetails;
    }

    private String getRelativePath(String filePath) {
        if (filePath.startsWith(localPath)) {
            filePath = filePath.substring(localPath.length());
        }
        return filePath;
    }

    @Override
    public String getAuthor() {
        String temp = author;
        author = null;
        return temp;
    }

}
