package com.thp.database.service;

import java.io.File;

public interface FileExecutionTracker {
    void updateFileExecutionStatus(File file, String service, Boolean status, String exception, String author);
}
