package com.thp.database.service;

import com.thp.common.utils.exceptions.THPException;

import java.io.File;
import java.sql.Connection;

public interface ConnectionManager {

    Connection getConnection(File service, String name) throws THPException;

    void closeConnection(Connection connection) throws THPException;

}