package com.thp.database.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sql_file_execution_history")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SQLFileExecutionHistory implements Serializable {
    private static final long serialVersionUID = -3190267095256906L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "service")
    private String service;

    @Column(name = "operation_type")
    private String operationType;

    @Column(name = "execution_status")
    private Boolean status;

    @Column(name = "file_contents")
    private String fileContents;

    @Column(name = "exception")
    private String exception;

    @Column(name = "author")
    private String author;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_timestamp")
    private Date createdTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_timestamp")
    private Date modifiedTimestamp;


}
