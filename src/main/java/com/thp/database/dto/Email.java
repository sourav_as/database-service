package com.thp.database.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Email {
    private Integer errorCode;
    private String errorMessage;
    private FileDetails fileDetails;
    private CommitDetails commitDetails;

    @Override
    public String toString() {
        return "Error Details: " + '\n' +
                "\terrorCode = " + errorCode + '\n' +
                "\terrorMessage = " + errorMessage + '\n' +
                fileDetails + '\n' +
                commitDetails + '\n';
    }
}
