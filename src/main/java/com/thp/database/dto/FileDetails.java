package com.thp.database.dto;

import com.thp.database.constants.Constants;
import lombok.Data;

@Data
public class FileDetails {

    private String name;
    private String path;
    private String database;

    public FileDetails() {
        name = Constants.NOT_APPLICABLE;
        path = Constants.NOT_APPLICABLE;
        database = Constants.NOT_APPLICABLE;
    }

    public FileDetails(String name, String path, String database) {
        this.name = name;
        this.path = path;
        this.database = (database == null || database.isEmpty()) ? Constants.NOT_APPLICABLE : database;
    }

    @Override
    public String toString() {
        return '\n' + "File Details:" + '\n' +
                "\tname = " + name + '\n' +
                "\tpath = " + path + '\n' +
                "\tdatabase = " + database;
    }
}