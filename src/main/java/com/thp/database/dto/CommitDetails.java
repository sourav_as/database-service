package com.thp.database.dto;

import com.thp.database.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CommitDetails {
    private String author;
    private String email;
    private String time;
    private String message;

    public CommitDetails() {
        author = Constants.NOT_APPLICABLE;
        email = Constants.NOT_APPLICABLE;
        time = Constants.NOT_APPLICABLE;
        message = Constants.NOT_APPLICABLE;
    }

    @Override
    public String toString() {
        return '\n' + "Commit Details:" + '\n' +
                "\tauthor = " + author + '\n' +
                "\temail = " + email + '\n' +
                "\ttime = " + time + '\n' +
                "\tmessage = " + message + '\n';
    }
}
