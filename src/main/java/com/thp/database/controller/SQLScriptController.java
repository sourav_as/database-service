package com.thp.database.controller;

import com.thp.common.utils.exceptions.THPException;
import com.thp.common.utils.service.ResponseHelper;
import com.thp.common.utils.utility.PropertyReader;
import com.thp.database.constants.Constants;
import com.thp.database.constants.SuccessCodes;
import com.thp.database.service.FileManager;
import com.thp.database.service.GitManager;
import com.thp.database.service.ScriptExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.File;

@RestController
@Slf4j
@RequestMapping(Constants.REQ_MAPPING_EXECUTE_SCRIPT)
public class SQLScriptController {

    @Autowired
    @Qualifier("sqlScriptExecutor")
    private ScriptExecutor sqlScriptExecutor;

    @Autowired
    @Qualifier("refreshScriptExecutor")
    private ScriptExecutor refreshScriptExecutor;

    @Autowired
    private FileManager fileManager;
    @Autowired
    private GitManager gitManager;

    @GetMapping(value = "/v1/script/execute")
    @PostConstruct
    public ResponseEntity<Object> executeScript() throws THPException {
        log.info("Starting SQL Script Executor.");
        try {
            gitManager.initialize();
            executeServices();
            refreshScriptExecutor.execute(fileManager.getRefreshScript(), "");
            gitManager.terminate();
        } catch (THPException e) {
            log.error(e.getMessage());
            return ResponseHelper.generateResponse(e.getStatusCode(),
                    e.getMessage());
        }
        return ResponseHelper.generateResponse(SuccessCodes.SCRIPT_EXECUTION_SUCCESS.getSuccessCode(),
                SuccessCodes.SCRIPT_EXECUTION_SUCCESS.getSuccessMsg());
    }

    private void executeServices() throws THPException {
        File[] services = fileManager.getServicesList();
        for (File service : services) {
            log.info("Executing service: {}", service.getName());
            if (Constants.CLINICS.equals(service.getName())) {
                executeClinicsFlow(service);
            } else {
                sqlScriptExecutor.execute(service, "");
            }
        }
    }

    private void executeClinicsFlow(File service) throws THPException {
        File[] clinicServices = fileManager.sort(service.listFiles(File::isDirectory));
        for (File clinicService : clinicServices) {
            log.info("Executing clinic service: {}", clinicService.getName());
            if (clinicService.getName().equals(Constants.GLOBAL_SERVICE)) {
                sqlScriptExecutor.execute(clinicService, Constants.CLINICS_GLOBAL);
            } else {
                for (String tenant : PropertyReader.getProperty(Constants.TENANT_DS_NAMES).split(Constants.SEPARATOR)) {
                    sqlScriptExecutor.execute(clinicService, tenant);
                }
            }
        }
    }

}
