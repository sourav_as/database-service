DROP PROCEDURE IF EXISTS dml_custom_insert_procedure;
---
CREATE PROCEDURE dml_custom_insert_procedure()
 LANGUAGE plpgsql AS
 $$
 DECLARE
    data_exists INT;
 BEGIN
	SELECT COUNT(1) FROM testpersons1 where id = 10 AND name = 'test10' INTO data_exists;
    IF data_exists = 0 THEN
        INSERT INTO testpersons1 values (10,'test10','');
        INSERT INTO testpersons1 values (11,'test11','');
    END IF;
 END
$$;
---
CALL dml_custom_insert_procedure();
---
DROP PROCEDURE IF EXISTS dml_custom_insert_procedure;
---