DROP PROCEDURE IF EXISTS dml_custom_insert_procedure;
---
CREATE PROCEDURE `dml_custom_insert_procedure`()
BEGIN
DECLARE data_exists INT; 
	SET data_exists = (SELECT COUNT(1) FROM Persons where id = 10 AND name = 'test10');
    IF data_exists = 0 THEN
        INSERT INTO Persons values (10,'test10','');
        INSERT INTO Persons values (11,'test11','');
    END IF;
END
---
call dml_custom_insert_procedure();
---
drop procedure dml_custom_insert_procedure;
---
drop procedure if exists dml_custom_insert_procedure1;
---
CREATE PROCEDURE `dml_custom_insert_procedure1`()
BEGIN
DECLARE data_exists INT;
	SET data_exists = (SELECT COUNT(1) FROM Persons where id = 12 AND name = 'test12');
    IF data_exists = 0 THEN
        INSERT INTO Persons values (12,'test12','');
        INSERT INTO Persons values (13,'test13','');
    END IF;
END
---
call dml_custom_insert_procedure1();
---
drop procedure dml_custom_insert_procedure1;
---